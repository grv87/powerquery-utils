section Section1;

shared fGetDOW = let
    Source = (d as date) as number => Date.DayOfWeek(d, Day.Monday)
in
    Source;

shared working_calendar = let
    get_working_calendar = (year as number) as table =>
        let
            Источник = Xml.Tables(Web.Contents("http://xmlcalendar.ru/data/ru/" & Number.ToText(year) & "/calendar.xml")),
            days = Источник{0}[days],
            day = days{0}[day],
            text_to_date = (t as nullable text) as nullable date => if t = null then null else #date(year, Number.FromText(Text.Range(t, 0, 2)), Number.FromText(Text.Range(t, 3, 2))),
            ColumnTypes = #table(type table[Name=text, Type=type], {{"d", type date}, {"t", Int64.Type}, {"h", Int64.Type}, {"f", type date}}),
            #"Переименованные столбцы" = Table.RenameColumns(day, List.Transform(ColumnTypes[Name], each {"Attribute:" & _, _}), MissingField.Ignore),
            Columns = Table.Schema(#"Переименованные столбцы")[Name],
            #"Измененный тип1" = Table.TransformColumnTypes(#"Переименованные столбцы", Table.ToRows(Table.SelectRows(ColumnTypes, each [Type] <> type date and List.Contains(Columns, [Name])))),
            Пользовательский1 = Table.TransformColumns(#"Измененный тип1", List.Transform(Table.SelectRows(ColumnTypes, each [Type] = type date and List.Contains(Columns, [Name]))[Name], each {_, text_to_date, type date}))
        in
            Пользовательский1,
    Source = Table.Combine(List.Generate(() => 2013, each _ <= 2023, each _ + 1, each get_working_calendar(_))),
    WithKeys = Table.AddKey(Table.AddKey(Source, {"d"}, true), {"f"}, false),
    Buffered = Table.Buffer(WithKeys)
in
    Buffered;

shared fGetDayOfWeekFromWorkingCalendar = let
    Источник = (dd as date, work_week_length as number) as number =>
        let
            r = try working_calendar{[d=dd]},
            standard_dow = fGetDOW(dd)
        in
            // Для 7-дневной рабочей недели считаем, что праздников нет
            if work_week_length = 7 or work_week_length = null or r[HasError] then
                standard_dow
            else if r[Value][t] = 1 then 6 // Считаем праздники как Вс
            else if standard_dow >= work_week_length then fGetDOW(working_calendar{[f=dd]}[d])
            else standard_dow
in
    Источник;

shared working_calendars = let
    get_working_calendar = (year as number) as table =>
        let
            Источник = Xml.Tables(Web.Contents("http://xmlcalendar.ru/data/ru/" & Number.ToText(year) & "/calendar.xml")),
            #"Измененный тип" = Table.TransformColumnTypes(Источник,{{"Attribute:year", Int64.Type}, {"Attribute:lang", type text}, {"Attribute:date", type date}}),
            days = #"Измененный тип"{0}[days],
            day = days{0}[day],
            ColumnTypes = {{"Attribute:d", type text}, {"Attribute:t", Int64.Type}, {"Attribute:h", Int64.Type}, {"Attribute:f", type text}},
            Columns = Table.Schema(day)[Name],
            #"Измененный тип1" = Table.TransformColumnTypes(day,List.Select(ColumnTypes, each List.Contains(Columns, _{0}))),
            text_to_date = (t as nullable text) as nullable date => if t = null then null else #date(year, Number.FromText(Text.Range(t, 0, 2)), Number.FromText(Text.Range(t, 3, 2))),
            Пользовательский1 = Table.TransformColumns(#"Измененный тип1", List.Transform(List.Intersect({{"Attribute:d", "Attribute:f"}, Columns}), each {_, text_to_date, type date}))
        in
            Пользовательский1,
    Источник = Table.Combine(List.Generate(() => 2019, each _ <= 2023, each _ + 1, each get_working_calendar(_)))
in
    Источник;