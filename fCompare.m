section Section1;

shared fCompare = let
    Source = (expected as table, actual as table) as table =>
    let
        expected_with_present = Table.AddColumn(expected, "Present", each true, type logical),
        actual_with_present = Table.AddColumn(actual, "Present", each true, type logical),
        expected_type = Value.Type(expected_with_present),
        key_columns = fPrimaryKeyColumns(expected_type),
        fPrefix = (prefix as text, s as text) as text => prefix & "." & s,
        expected_prefix = "Эталон",
        actual_prefix = "Расч",
        expected_columns = List.RemoveItems(Type.TableSchema(expected_type)[Name], key_columns),
        actual_columns = List.RemoveItems(Type.TableSchema(Value.Type(actual_with_present))[Name], key_columns),
        common_columns = List.Intersect({expected_columns, actual_columns}),
        joined = Table.Join(
            Table.PrefixColumns(expected_with_present, expected_prefix),
            List.Transform(key_columns, each fPrefix(expected_prefix, _)),
            Table.PrefixColumns(actual_with_present, actual_prefix),
            List.Transform(key_columns, each fPrefix(actual_prefix, _)),
            JoinKind.FullOuter
        ),
        with_merged_key = Table.AddKey(
            List.Accumulate(
                key_columns,
                joined,
                (state as table, current as text) as table =>
                    let
                        expected = fPrefix(expected_prefix, current),
                        actual = fPrefix(actual_prefix, current)
                    in
                        Table.RemoveColumns(Table.AddColumn(state, current, each Record.Field(_, expected) ?? Record.Field(_, actual), Type.TableColumn(expected_type, current)), {expected, actual})
            ),
            key_columns,
            true
        )
    in
        Table.Sort(
            Table.ReorderColumns(
                with_merged_key,
                key_columns & List.Combine(List.Transform(common_columns, each {fPrefix(expected_prefix, _), fPrefix(actual_prefix, _)})) & List.Transform(List.RemoveItems(expected_columns, common_columns), each fPrefix(expected_prefix, _)) & List.Transform(List.RemoveItems(actual_columns, common_columns), each fPrefix(actual_prefix, _))
            ),
            List.Transform(key_columns, each {_, Order.Ascending})
        )
in
    Source;
