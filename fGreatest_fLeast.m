section Section1;

shared fGreatest = let
    Источник = (d1 as any, d2 as any) as any => if d1 >= d2 then d1 else d2
in
    Источник;

shared fLeast = let
    Источник = (d1 as any, d2 as any) as any => if d1 <= d2 then d1 else d2
in
    Источник;
