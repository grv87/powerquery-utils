section Section1;

shared fGroupToSingleRows = let
    Source = (t as table, key as list) as table =>
        let
            t_type = Value.Type(t),
            columns = List.RemoveItems(Type.TableSchema(t_type)[Name], key)
        in
            Table.Group(t, key, List.Transform(columns, (column) => {column, (values) => List.Single(List.Distinct(Table.Column(values, column))), Type.TableColumn(t_type, column)}))
in
    Source;
