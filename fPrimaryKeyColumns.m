section Section1;

shared fPrimaryKeyColumns = let
    Source = (t as any) as list =>
        let
            tp = if t is type then t else Value.Type(t)
        in
            List.Single(List.Select(Type.TableKeys(tp), each [Primary] = true))[Columns]
in
    Source;
