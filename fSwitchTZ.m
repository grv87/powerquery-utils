section Section1;

shared fSwitchTZ = let
    Источник = (tz as nullable number, dtz as any) as nullable datetimezone =>
        if tz = null then
            null
        else if Value.Is(dtz, DateTimeZone.Type) then
            DateTimeZone.SwitchZone(dtz, 0, tz)
        else
            DateTime.AddZone(DateTime.From(dtz), 0, tz)
in
    Источник;
