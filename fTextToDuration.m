section Section1;

shared fTextToDuration = let
    Источник = (t as nullable text) as nullable duration => let l = List.Transform(Text.Split(t, ":"), Number.From) in if t = null then null else #duration(0, l{0}, l{1}, 0)
in
    Источник;
