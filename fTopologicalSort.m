section Section1;

shared fTolopoligalSort = let
    Источник = (vertices as list, edges as table) as table =>
        let
            VerticesListToTable = (vertices as list) as table => Table.FromList(vertices, Splitter.SplitByNothing(), type table[ТаблицаПредставление = text])
        in
            // Алгоритм Кана
            Table.AddIndexColumn(VerticesListToTable(List.RemoveFirstN(List.Generate(
                () => [
                    vertices_table = VerticesListToTable(vertices),
                    result = null,
                    edges_table = edges
                ],
                each [result] <> null or Table.RowCount([vertices_table]) > 0,
                each let
                    v = try Table.First(Table.SelectRows(Table.Join([vertices_table], "ТаблицаПредставление", [edges_table], "Цель", JoinKind.LeftOuter), each [Источник] = null))[ТаблицаПредставление] otherwise null
                in [
                    vertices_table = Table.SelectRows([vertices_table], each [ТаблицаПредставление] <> v),
                    result = v,
                    edges_table = Table.SelectRows([edges_table], each [Источник] <> v)
                ],
                each [result]
            ))), "order_num")
in
    Источник;
