section Section1;

shared fYesOrNoToLogical = let
    Источник = (t as nullable text) as nullable logical => if t = null then null else let s = Text.Lower(t) in if s = "да" then true else if s = "нет" then false else error [Reason = "Invalid value", Detail = [value = t]]
in
    Источник;
